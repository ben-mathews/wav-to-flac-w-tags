import sys
import os
import time
import shutil
import glob
import re
import argparse
import logging
import progressbar
from pydub import AudioSegment

class FileToProcess:
    """
    Class to hold a file to process.  If it's a wav file we convert and tag it.  If it's something else, we just copy it.
    """
    def __init__(self, original_filename, new_filename, artist, album, year, tracknumber, title):
        """
        Constructor
        :param original_filename: Original path + filename
        :param new_filename: New path + filename
        :param artist: Artist
        :param album: Album
        :param year: Year (str)
        :param tracknumber: Track number (str)
        :param title: Track title
        """
        self.original_filename = original_filename
        self.original_filename_basename = os.path.basename(original_filename)
        self.new_filename = new_filename
        _, self.new_filename_extension = os.path.splitext(new_filename)
        self.new_filename_basename = os.path.basename(new_filename)
        self.artist = artist
        self.album = album
        self.year = year
        self.tracknumber = tracknumber
        self.tracktotal = 0
        self.title = title

    def process_file(self):
        """
        Processes the file
        :return: Void
        """
        if not os.path.exists(os.path.dirname(self.new_filename)):
            os.makedirs(os.path.dirname(self.new_filename))
        if self.new_filename_extension == '.flac':
            file = AudioSegment.from_file(self.original_filename)
            file.export(out_f=self.new_filename, format='flac', tags={'ARTIST': self.artist, 'ALBUMARTIST': self.artist, 'YEAR': self.year, 'DATE': self.year, 'ALBUM': self.album, 'TRACKNUMBER': self.tracknumber, 'TITLE': self.title})
        else:
            shutil.copy2(self.original_filename, self.new_filename)

def process_arguments():
    """
    Processes input arguments
    """
    # Parse the command line
    argparser = argparse.ArgumentParser(
        description=
        (
            "Ben's script to process wav files and convert them to flac files with proper tags."
        ),
        add_help=False,  # Allows changing of help flag from -h to -H
    )
    argparser.add_argument(
        "--help",
        "-H",
        action="help",
        help="show this help message and exit",
    )
    argparser.add_argument(
        "--source-folder",
        required=True,
        action="store",
        default='',
        help="source folder for wav files",
    )
    argparser.add_argument(
        "--destination-folder",
        required=True,
        action="store",
        default='',
        help="source folder for wav files",
    )
    argparser.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        help="perform a trial run with no changes made",
    )

    args = argparser.parse_args()

    return args


def progress(count, total, suffix=''):
    """
    Implements a progress bar - ripped off from somewhere
    """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben


def process_folder(source_folder, destination_folder, dry_run):
    """
    Processes a folder to move and rename files
    :param source_folder:
    :param destination_folder:
    :param show_name:
    :param min_file_size_mb:
    :return:
    """
    all_files_to_process = []
    processed_folders = []
    unprocessed_folders = []

    root_folder_contents = glob.glob(source_folder + '/*')
    for artist_folder in root_folder_contents:
        if os.path.isfile(artist_folder): # Ignore files in the root folder
            continue
        artist = os.path.basename(artist_folder)
        artist_folder_contents = glob.glob(artist_folder + '/*')
        for album_folder in artist_folder_contents:
            if os.path.isfile(album_folder):
                logging.exception('Error, not expecting any files in artists folder.  Not processing folder: ' + album_folder)
                unprocessed_folders.append(album_folder)
                continue
            year = None
            album = None
            match = re.search('(?P<year>\d+) - (?P<album>.*)', os.path.basename(album_folder))
            if match is None:
                catchme = 1
            try:
                year = match.group('year')
            except Exception as e:
                logging.exception('Exception caught when trying to get Year: ' + str(e))
                logging.exception('Not processing folder: ' + album_folder)
                unprocessed_folders.append(album_folder)
                continue
            try:
                album = match.group('album')
            except Exception as e:
                logging.exception('Exception caught when trying to get Album: ' + str(e))
                logging.exception('Not processing folder: ' + album_folder)
                unprocessed_folders.append(album_folder)
                continue

            album_files = sorted(glob.glob(album_folder + '/*'))
            files_to_process = []
            all_files_processed_successfully = True
            for file in album_files:
                if not os.path.isfile(file):
                    logging.exception('Error, not expecting any subfolders in album folder.  Not processing folder: ' + album_folder)
                    all_files_processed_successfully = False
                    continue
                if 'volumeID' in file: # Ignore the volumeID.* files from the CDex rip
                    continue
                tracknumber = None
                title = None
                filename, extension = os.path.splitext(file)
                if extension.lower() == '.wav':
                    match = re.search('(?P<tracknumber>\d+) - (?P<title>.*)', os.path.basename(file.replace(extension, '')))
                    try:
                        tracknumber = match.group('tracknumber')
                    except Exception as e:
                        logging.exception('Exception caught when trying to get Track Number: ' + str(e))
                        logging.exception('Not processing file: ' + file)
                        all_files_processed_successfully = False
                        continue
                    try:
                        title = match.group('title')
                    except Exception as e:
                        logging.exception('Exception caught when trying to get Title: ' + str(e))
                        logging.exception('Not processing file: ' + file)
                        all_files_processed_successfully = False
                        continue
                    new_filename = file.replace(source_folder, destination_folder).replace(extension, '.flac')
                else:
                    new_filename = file.replace(source_folder, destination_folder)

                files_to_process.append(FileToProcess(original_filename=file, new_filename=new_filename, artist=artist, year=year, album=album, tracknumber=tracknumber, title=title))
            if all_files_processed_successfully:
                processed_folders.append(album_folder)
                tracktotal = max([int(file_to_process.tracknumber) for file_to_process in files_to_process if file_to_process.tracknumber is not None])
                for file_to_process in files_to_process:
                    if file_to_process.tracknumber is not None:
                        file_to_process.tracktotal = tracktotal
                    all_files_to_process.append(file_to_process)
            else:
                unprocessed_folders.append(album_folder)

    max_original_file_name = 0
    max_new_file_name = 0
    max_artist_len = 0
    max_year_len = 0
    max_album_len = 0
    max_tracknum_len = 0
    max_title_len = 0

    for file in all_files_to_process:
        filename, extension = os.path.splitext(file.new_filename)
        if len(file.original_filename_basename) > max_original_file_name:
            max_original_file_name = len(file.original_filename_basename)
        if len(file.new_filename_basename) > max_new_file_name:
            max_new_file_name = len(file.new_filename_basename)
        if extension == '.flac':
            if len(file.artist) > max_artist_len:
                max_artist_len = len(file.artist)
            if len(file.year) > max_year_len:
                max_year_len = len(file.year)
            if len(file.album) > max_album_len:
                max_album_len = len(file.album)
            if len(file.tracknumber) > max_tracknum_len:
                max_tracknum_len = len(file.tracknumber)
            if len(file.title) > max_title_len:
                max_title_len = len(file.title)

    print('Files to be processed:')
    #top_row_text = 'Orig File Name' + (max_original_file_name-len('Orig File Name')+3)*' '+ 'New File Name' + (max_new_file_name-len('New File Name')+3)*' '+ 'Artist' + (max_artist_len-len('Artist')+3)*' '+ 'Year' + (max_year_len-len('Year')+3)*' '+ 'Album' + (max_album_len-len('Album')+3)*' '+ 'Num' + (max_tracknum_len-len('Num')+3)*' '+  'Title' + (max_title_len-len('Title')+3)*' '
    top_row_text = 'Artist' + (max_artist_len-len('Artist')+3)*' '+ 'Year' + (max_year_len-len('Year')+3)*' '+ 'Album' + (max_album_len-len('Album')+3)*' '+ 'Num' + (max_tracknum_len-len('Num')+3)*' '+  'Title' + (max_title_len-len('Title')+3)*' '
    print(top_row_text)
    for file in all_files_to_process:
        filename, extension = os.path.splitext(file.new_filename)
        if extension == '.flac':
            #row_text = f'{file.original_filename_basename: <{max_original_file_name+3}}{file.new_filename_basename: <{max_new_file_name+3}}{file.artist: <{max_artist_len+3}}{file.year: <{max_year_len+3}}{file.album: <{max_album_len+3}}{file.tracknumber: <{max_tracknum_len+3}}{file.title: <{max_title_len}}'
            row_text = f'{file.artist: <{max_artist_len+3}}{file.year: <{max_year_len+3}}{file.album: <{max_album_len+3}}{file.tracknumber: <{max_tracknum_len+3}}{file.title: <{max_title_len}}'
            print(row_text)
        #else:
        #    row_text = f'{file.original_filename_basename: <{max_original_file_name + 3}}{file.new_filename_basename: <{max_new_file_name + 3}}'


    if args.dry_run == False:
        progress(0, len(all_files_to_process), 'Starting')
        for idx, file in enumerate(all_files_to_process):
            file.process_file()
            time.sleep(0.01)
            progress(idx, len(all_files_to_process), file.original_filename + 100*' ')
        print('Done processing files')



if __name__ == "__main__":
    args = process_arguments()
    assert os.path.isdir(args.source_folder), "Source folder does not exist"
    assert os.path.isdir(args.destination_folder), "Source folder does not exist"
    process_folder(source_folder=args.source_folder, destination_folder=args.destination_folder, dry_run=args.dry_run)
    logging.info('Done')