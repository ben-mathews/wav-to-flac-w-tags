# wav-to-flac-w-tags

Python script that converts a folder of ripped WAV files into FLAC files and applies Artist, Album, Year, and Title tags based directory structure.  Assumes files are in a directory with structure MUSIC/$ARTIST/$YEAR - $ALBUM/$TRACKNUM - $TITLE.wav.